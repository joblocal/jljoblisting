<?php
require_once 'bootstrap.php';

$vacancyList = $api->get('/api/vacancy', array('published' => 1, 'page' => 1, 'page_size' => 50, 'sort' => 'publish_date.desc'));
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Joblocal Joblisting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<h1>Stellenanzeigen</h1>

		<div>Anzahl Stellenanzeigen gesamt: <?php echo $api->get('/api/vacancy/count'); ?></div>
		<table border="1">
		<?php foreach ($vacancyList as $index => $vacancy): ?>
			<?php if ($index == 0): ?>
			<tr>
				<?php foreach ($vacancy as $title => $column): ?>
				<th><?php echo htmlspecialchars($title); ?></th>
				<?php endforeach; ?>				
			</tr>
			<?php endif; ?>
			<tr>
				<?php foreach ($vacancy as $column): ?>
				<td><?php echo htmlspecialchars($column); ?></td>
				<?php endforeach; ?>				
			</tr>
		<?php endforeach; ?>
		</table>



	</body>
</html>
