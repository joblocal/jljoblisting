<?php
require_once 'bootstrap.php';

$vacancyList = null;
// Sollte die APC Erweiterung geladen sein, die Jobs für 10min zwischenspeichern
if (extension_loaded('apc'))
	$vacancyList = apc_fetch('vacancylist');

if (!$vacancyList)
{
	$vacancyList = $api->get('/api/vacancy', array(
		'published' => 1,				// Nur veröffentlichte Stellenanzeigen (30 Tage Laufzeit)
		'page' => 1,					// erste Seite
		'page_size' => 50,				// 50 Einträge
		'sort' => 'publish_date.desc',	// Nach Veröffentlichungsdatum absteigend sortieren
		// 'company_id' => 5804,		// Nur Stellenanzeigen dieses Unternehmens anzeigen
		// 'title' => 'Koch',			// Nur Stellenanzeigen anzeigen, die im Titel oder der Beschreibung den Suchbegriff enthalten. Mehrere Begriffe werden mit ODER verknüpft
										// Weitere Filtermöglichkeiten unter /api des jeweiligen Marktes, z.B. https://rosenheimjobs.de/api
	));
	if ($vacancyList && extension_loaded('apc'))
		apc_store('vacancylist', $vacancyList, 10*60);
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Joblocal Joblisting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<div class="vacancyList">
			<?php foreach ($vacancyList as $data): ?>
			<article class="vacancy clearfix">
				<header>
					<h3>
						<a href="<?php echo $data['url']; ?>" title="<?php echo htmlspecialchars($data['title']); ?>">
							<?php echo htmlspecialchars($data['title'].(!empty($data['city'])?' in '.$data['city']:'')); ?>
						</a>
					</h3>
					<strong><?php echo htmlspecialchars($data['company']['name']); ?>, <?php echo htmlspecialchars($data['city']?$data['city']:$data['company']['city']); ?></strong>
					<em>
						<time datetime="<?php echo date(DATE_ISO8601, $data['publish_date']); ?>">veröffentlicht <?php echo date('d.m.Y', $data['publish_date']); ?></time>
					</em>
				</header>
				<div<?php if ($data['company']['file']) echo ' class="logo"'; ?>>
					<?php if ($data['shortDescription']): ?>
					<p><?php echo htmlspecialchars($data['shortDescription']); ?></p>
					<?php endif; ?>
				</div>
				<?php if ($data['company']['file']): ?>
				<footer>
					<a href="<?php echo $data['company']['url']; ?>" title="<?php echo htmlspecialchars($data['company']['name']); ?>">
						<img src="<?php echo $data['company']['file']['image']; ?>" alt="Logo <?php echo htmlspecialchars($data['company']['name']); ?>"><br>
					</a>
				</footer>
				<?php endif; ?>
			</article>
			<?php endforeach; ?>
		</div>


	</body>
</html>
