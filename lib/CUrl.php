<?php

if (!function_exists('curl_init'))
	throw new Exception('Curl Extension is missing!');

class CUrl {

	/**
	 * Filehandle vom Init.
	 *
	 * @var   resource Handle
	 */
	protected $handle;

	/**
	 * Konstruktor.
	 */
	public function __construct() {
		$this->handle = curl_init();
		$this->setOptions(
			array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_USERAGENT => 'CURL (PHP/'.phpversion().')',
				CURLOPT_TIMEOUT => ini_get('default_socket_timeout'),
			)
		);

	}

	/**
	 * Erstellt einen Handleklon.
	 *
	 * @return void
	 */
	public function __clone() {
		$this->handle = curl_copy_handle($this->handle);

	}

	/**
	 * Destruktor mit Rückgabe des Status.
	 */
	public function __destruct() {
		return curl_close($this->handle);

	}

	/**
	 * Setzt eine Option für curl.
	 *
	 * @param string $key   Schlüssel.
	 * @param string $value Wert.
	 *
	 * @return boolean Status
	 */
	public function setOption($key, $value) {
		return curl_setopt($this->handle, $key, $value);

	}

	/**
	 * Setzt mehrere Optionen für curl.
	 *
	 * @param array $array Liste der Optionen.
	 *
	 * @return void
	 */
	public function setOptions(array $array) {
		return curl_setopt_array($this->handle, $array);

	}

	/**
	 * Sendet eine Anfrage.
	 *
	 * @param CUrlRequest $request Request.
	 *
	 * @return CUrlResponse Response
	 */
	public function send(CUrlRequest $request, $responseClass = 'CUrlResponse') {
		$response = new $responseClass();
		$response->setRequest($request);
		$this->setOption(CURLOPT_URL, $request->getUrl());
		$this->setOption(CURLOPT_HTTPHEADER, $request->getCurlHeaders());

		switch ($request->getMethod()) {
			case CUrlRequest::METHOD_GET:
				$this->setOption(CURLOPT_HTTPGET, true);
				break;

			case CUrlRequest::METHOD_POST:
				$this->setOption(CURLOPT_POST, true);
				break;

			case CUrlRequest::METHOD_PUT:
				$this->setOption(CURLOPT_CUSTOMREQUEST, 'PUT');
				break;

			case CUrlRequest::METHOD_DELETE:
				$this->setOption(CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;

			case CUrlRequest::METHOD_HEAD:
				$this->setOption(CURLOPT_NOBODY, true);
				$this->setOption(CURLOPT_CUSTOMREQUEST, 'HEAD');
				break;

			default:
				throw new Exception('Method not found');
		}

		if($request->getBody()) {
			// Dank dem dirty Hack in Curl gibt es keine andere Möglichkeit:
			$postData = $request->getBody();
			if (is_array($postData))
			{
				$postAsString = true;
				foreach ($postData as $var)
				{
					if (is_string($var) && $var[0] === '@') {
						$postAsString = false;
						break;
					}
				}
				if ($postAsString)
					$postData = http_build_query($request->getBody());
			}
			$this->setOption(CURLOPT_POSTFIELDS, $postData);
		}

		$this->setOptions(
			array(
				CURLOPT_HEADERFUNCTION => array(
					$response,
					'addHeader',
				),
				CURLOPT_WRITEFUNCTION => array(
					$response,
					'addBody',
				),
			)
		);
		curl_exec($this->handle);

		if (curl_errno($this->handle)) {
			throw new Exception(curl_error($this->handle), curl_errno($this->handle));
		}
		return $response;

	}
}
