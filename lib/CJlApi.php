<?php
/*
 * CJlApi - Joblocal GmbH REST Api Wrapper
 * 
 * @author Daniel Pieper <daniel.pieper@joblocal.de>
 * @copyright Copyright (c) 2012 joblocal GmbH
 */

/**
 * Beschreibung der API unter /api des jeweiligen Marktes
 * @link https://rosenheimjobs.de/api
 * @link https://augsburgerjobs.de/api
 */
class CJlApi
{
	/**
	 * @var string Öffentlicher Schlüssel
	 */
	public $publicKey;
	/**
	 * @var string Privater Schlüssel
	 */
	public $privateKey;
	/**
	 * @var string Pfad zur Zertifikat-Datei
	 */
	public $certificateFile;
	/**
	 * @var string URL zur jeweiligen Website (z.B. https://rosenheimjobs.de)
	 */
	public $host;

	/**
	 * Setzt einen GET Request ab
	 *
	 * @param string $resource Ressource z.B. /api/vacancy
	 * @param array $params Parameter
	 * @return array Daten
	 */
	public function get($resource, $params = array())
	{
		return $this->send('get', $resource, $params);
	}

	/**
	 * Setzt einen POST Request ab
	 *
	 * @param string $resource Ressource z.B. /api/vacancy/123
	 * @param array $params Parameter
	 * @return array Daten
	 */
	public function post($resource, $params = array())
	{
		return $this->send('post', $resource, $params);
	}

	/**
	 * Setzt einen PUT Request ab
	 *
	 * @param string $resource Ressource z.B. /api/vacancy/123
	 * @param array $params Parameter
	 * @return array Daten
	 */
	public function put($resource, $params = array())
	{
		return $this->send('put', $resource, $params);
	}

	/**
	 * Setzt einen DELETE Request ab
	 *
	 * @param string $resource Ressource z.B. /api/vacancy/123
	 * @param array $params Parameter
	 * @return array Daten
	 */
	public function delete($resource, $params = array())
	{
		return $this->send('delete', $resource, $params);
	}

	/**
	 * Setzt einen Request ab
	 *
	 * Über die Parameter sowie die aktuelle Zeit wird eine Signatur erstellt,
	 * die zusätzlich zu den Public/Private Keys überprüft wird.
	 * Falls schwerwiegende Fehler auftreten wird eine Exception geworfen
	 * (davon ausgeschlossen sind z.B. Feldvalidierungen).
	 *
	 * @param string $method Methode (GET, POST, PUT, DELETE)
	 * @param string $resource Ressource z.B. /api/vacancy
	 * @param array $params Parameter
	 * @return array Daten
	 */
	public function send($method, $resource, $params = array())
	{
		$method = strtoupper($method);
		if (!in_array($method, array('GET', 'POST', 'PUT', 'DELETE')))
			throw new Exception ('Invalid Method');

		$date = date(DATE_RFC822);

		$sigData = $params;
		unset($sigData['id']);
		unset($sigData['format']);
		// Dateiuploads mit CUrl: http://dtbaker.com.au/random-bits/uploading-a-file-using-curl-in-php.html
		// Der *magische* Parameter darf deshalb nicht in der Signatur auftauchen
		foreach ($sigData as $key => $value)
		{
			if (is_string($value) && $value[0] === '@')
				unset($sigData[$key]);
		}
		ksort($sigData);
		$sigDataStr = implode("\n", array($method, $resource, http_build_query($sigData), $date));

		$signature = hash_hmac('sha256', $sigDataStr, $this->privateKey);

		$url = $this->host.$resource;

		$req = new CUrlRequest($url);
		switch ($method)
		{
			case 'GET':
				$req->setMethod(CUrlRequest::METHOD_GET);
				if (count($params) > 0)
				{
					$url .= '?'.http_build_query($params);
					$req->setUrl($url);
				}
				break;

			case 'POST':
				$req->setMethod(CUrlRequest::METHOD_POST);
				$req->setBody($params);
				break;

			case 'PUT':
				$req->setMethod(CUrlRequest::METHOD_PUT);
				$req->setBody(http_build_query($params));
				break;

			case 'DELETE':
				$req->setMethod(CUrlRequest::METHOD_DELETE);
				$req->setBody(http_build_query($params));
				break;
		}
		$req->addHeader('Date', $date);
		$req->addHeader('Authentication', $this->publicKey.':'.$signature);

		$curl = new CUrl();
		$curl->setOption(CURLOPT_SSL_VERIFYPEER, true);

//		0: Don’t check the common name (CN) attribute
//		1: Check that the common name attribute at least exists
//		2: Check that the common name exists and that it matches the host name of the server
		$curl->setOption(CURLOPT_SSL_VERIFYHOST, 2);
		$curl->setOption(CURLOPT_CAINFO, $this->certificateFile);

		$response = $curl->send($req);
		$format = (isset($params['format'])?$params['format']:'json');

		if ((int)$response->getHeader('Status-Code') >= 400)
		{
			$message = 'Unbekannter Fehler: '.$response->getBody();

			if ($format == 'xml' && $xml = DOMDocument::loadXML($response->getBody()))
			{
				if ($xml->getElementsByTagName('status')->length > 0 && $xml->getElementsByTagName('status')->item(0)->nodeValue == 'nok')
					$message = $xml->getElementsByTagName('message')->item(0)->nodeValue;
			}
			elseif ($format == 'json' && $jsondata = json_decode($response->getBody(), true))
			{
				if (isset($jsondata['status']) && $jsondata['status'] == 'nok')
					$message = $jsondata['message'];
			}

			throw new Exception($message, (int)$response->getHeader('Status-Code'));
		}

		if ($format == 'xml')
			return DOMDocument::loadXML($response->getBody());
		elseif ($format == 'json')
			return json_decode($response->getBody(), true);
		else
			return $response;
	}



}