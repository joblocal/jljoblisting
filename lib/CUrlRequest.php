<?php

class CUrlRequest {

	/**
	 * Get Methode.
	 *
	 */
	const METHOD_GET = 1;

	/**
	 * Post Methode.
	 *
	 */
	const METHOD_POST = 2;

	/**
	 * Put Methode.
	 *
	 */
	const METHOD_PUT = 3;

	/**
	 * Head Methode.
	 *
	 */
	const METHOD_HEAD = 4;

	/**
	 * Delete Methode.
	 *
	 */
	const METHOD_DELETE = 5;

	/**
	 * Url.
	 *
	 * @var   string
	 */
	protected $url;

	/**
	 * Query.
	 *
	 * @var   $query
	 */
	protected $query = array();

	/**
	 * Header der Anfrage.
	 *
	 * @var   array
	 */
	protected $headers = array();

	protected $body = null;

	protected $method = self::METHOD_GET;

	/**
	 * Konstruktor.
	 *
	 * @param string $url   Url.
	 * @param array  $query Query-Array.
	 */
	public function __construct($url) {
		$this->url = $url;
	}

	/**
	 * Gibt die Url zurück.
	 *
	 * @return string Url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Setzt die Url
	 *
	 * @param string $url  Url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * Fügt einen von der Anfrage akzeptierten Content-Type hinzu.
	 *
	 * @param string $mimetypes,... Content-Type.
	 *
	 * @return void
	 */
	public function addAccept() {
		$mimetypes = func_get_args();
		foreach($mimetypes as $mimetype) {
			$this->headers['Accept'][] = $mimetype;
		}

	}

	/**
	 * Setzt einen Header für die Anfrage.
	 *
	 * @param string $key   Name.
	 * @param string $value Wert.
	 *
	 * @return void
	 */
	public function setHeader($key, $value) {
		$this->headers[$key] = array($value);

	}

	/**
	 * Fügt einen Header zur Anfrage hinzu.
	 *
	 * @param string $key   Name.
	 * @param string $value Wert.
	 *
	 * @return void
	 */
	public function addHeader($key, $value) {
		$this->headers[$key] = $value;

	}

	/**
	 * Gibt einen Header zurück.
	 *
	 * @param string $key Name.
	 *
	 * @return string Wert
	 */
	public function getHeader($key) {
		if (isset($this->headers[$key])) {
			return $this->headers[$key];
		}
		return false;

	}

	/**
	 * Setzt die Methode.
	 *
	 * Siehe Konstanten.
	 *
	 * @param integer $method Methode.
	 *
	 * @return void
	 */
	public function setMethod($method) {
		if (!in_array($method, array(self::METHOD_GET, self::METHOD_POST, self::METHOD_PUT, self::METHOD_DELETE, self::METHOD_HEAD))) {
			throw new Exception('Method not found');
		}
		$this->method = $method;

	}

	/**
	 * Liefert die Methode.
	 *
	 * @return integer Methode
	 */
	public function getMethod() {
		return $this->method;

	}

	/**
	 * Setzt den Body.
	 *
	 * @param string|array $body Body.
	 *
	 * @return void
	 */
	public function setBody($body) {
		$this->body = $body;

	}

	/**
	 * Liefert den Body.
	 *
	 * @return string|array Body
	 */
	public function getBody() {
		return $this->body;

	}

	/**
	 * Gibt die Header für Curl zurück.
	 *
	 * @return array Curl Headers
	 */
	public function getCurlHeaders() {
		$headers = array();
		foreach($this->headers as $key => $value) {
			if(is_array($value)) {
				$value = implode(',', $value);
			}
			$headers[] = $key.': '.$value;
		}
		return $headers;

	}

	/**
	 * Sendet die Anfrage.
	 *
	 * @return CUrlResponse Response
	 */
	public function send($responseClass = 'CUrlResponse') {
		$curl = new CUrl();
		return $curl->send($this, $responseClass);

	}

}
