<?php

class CUrlResponse {

	/**
	 * Request Objekt.
	 *
	 * @var   CUrlRequest Request
	 */
	protected $request;

	/**
	 * Header der Antwort.
	 *
	 * @var   array Header der Antwort.
	 */
	protected $headers = array();

	/**
	 * Inhalt der Antwort.
	 *
	 * @var   string Inhalt.
	 */
	protected $body = '';

	public function __toString() {
		try {
			return $this->getBody();
		}
		catch(Exception $e) {
			return '';
		}
	}

	/**
	 * Setzt das Request Objekt.
	 *
	 * @param CUrlRequest $request Request Objekt.
	 *
	 * @return void
	 */
	public function setRequest(CUrlRequest $request) {
		$this->request = $request;

	}

	/**
	 * Callback für Curl.
	 *
	 * @param mixed  $handle Curl-Handle.
	 * @param string $data   Daten.
	 *
	 * @return integer Länge der Daten
	 */
	public function addHeader($handle, $data) {
		if(!trim($data)) {
			return strlen($data);
		}
		if(substr($data, 0, 7) != 'HTTP/1.') {
			$part = explode(':', $data, 2);
			$part = array_map('trim', $part);
			$this->headers[$part[0]] = $part[1];
		} else {
			$part = explode(' ', $data, 3);
			$part = array_map('trim', $part);
			$this->headers['Status-Protocol'] = $part[0];
			$this->headers['Status-Code'] = $part[1];
			$this->headers['Status-Message'] = $part[2];
		}
		return strlen($data);

	}

	/**
	 * Gibt einen Header zurück.
	 *
	 * @param string $key Name.
	 *
	 * @return string Wert
	 */
	public function getHeader($key) {
		if (isset($this->headers[$key])) {
			return $this->headers[$key];
		}
		return null;

	}

	/**
	 * Liefert alle Header zurück.
	 *
	 * @return array Header
	 */
	public function getHeaders() {
		return $this->headers;

	}

	/**
	 * Liefert den Mimetype.
	 *
	 * @return string Mimetype
	 */
	public function getMimetype() {
		$part = explode(';', $this->getHeader('Content-Type'), 2);
		return $part[0];

	}

	/**
	 * Callback für Curl.
	 *
	 * @param mixed  $handle Curl-Handle.
	 * @param string $data   Daten.
	 *
	 * @return integer
	 */
	public function addBody($handle, $data) {
		$this->body .= $data;
		return strlen($data);

	}

	/**
	 * Gibt die Antwort zurück.
	 *
	 * @return string Inhalt
	 */
	public function getBody() {
		$mimetype = $this->getMimetype();
		if(!$this->checkAccept($mimetype)) {
			return null;
		}
		return $this->body;

	}

	/**
	 * Gibt die Antwort zurück.
	 *
	 * @return string Inhalt
	 */
	public function getParsedBody() {
		$body = $this->getBody();
		$mimetype = $this->getMimetype();
		switch($mimetype) {
			case 'application/javascript':
			case 'text/javascript':
			case 'application/json':
				return json_decode($body);
				break;
			case 'application/x-www-form-urlencoded':
				parse_str($body, $result);
				return $result;
				break;
		}
		return $body;

	}

	/**
	 * Gibt die unformatierte/gefilterte Antwort zurück.
	 *
	 * @return string Inhalt
	 */
	public function getUnsecureBody() {
		return $this->body;

	}

	public function checkAccept($mimetype) {
		$accepts = $this->request->getHeader('Accept');
		// Wenn keine Accept-Header definiert wurden ist das in Ordnung
		if(!is_array($accepts)) {
			return true;
		}
		foreach($accepts as $accept) {
			if(fnmatch($accept, $mimetype)) {
				return true;
			}
		}
		return false;
	}

}